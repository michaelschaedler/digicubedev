<?php
include '../data/functions.php';
include '../data/database_connection.php';

$database =  OpenCon();

?>

<head>

<!--font awesome--->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
</head>


<body>
    

</body>


<!-- FORMULAR  - START -------------------------------- -->
<div class="add-section"> 
    <form id="update-form" method="post"><!---../api.php?action=insert-->
        <!---hidden ID field---->
        <input id="ID" type="hidden"/><br>
        <!--------------->

        <h2 id="item-title">new item</h2><!--otherwise it gets overwritten--->

        <div class="child w50 form-item">
            <div class="w70">
            <h3>name*</h3>
            <input id="name" placeholder="name" type="text" name = "name"/><br>
            </div>
        </div>

        <div class="child w50 form-item">
            <div class="w70">
            <h3>url*</h3>
            <input id="url" placeholder="url" type="text" name = "url" /><br>
            </div>
        </div>

        <div class="child w50 form-item">
            <div class="w70">
            <h3>project*</h3>
            <select tabindex="1" id="project" name="project" >
              
            </select>          
            </div>
        </div>
        
        <div class="child w50 form-item">
            <div class="w70">
                <h3>type*</h3>
                <select tabindex="1" id="type" name="type" >
            
                </select>
            </div>
        </div>
        
        <div class="child w50 form-item">
            <div class="w70">

                <h3>status*</h3>
                <select tabindex="1" id="status" name="status" >
                    <option value="0">inactive</option>
                    <option value="1">active</option>
                </select>
            </div>

        </div> 

        <div class="child special-submit w50 form-item">
            <div class="w70">

            <input class="submit-button" id="save_or_edit" value="submit" type="submit" />
            </div>
        </div>



        </div> 

    </form>
    <div>