<?php
include '../data/functions.php';
include '../data/database_connection.php';
include '../data/upload.php';

$database =  OpenCon();

?>

<head>

<!--font awesome--->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
</head>


<body>
    

</body>


<!-- FORMULAR  - START -----------------action="../api.php?action=updateItem"-->
<div class="add-section"> 
    <form id="update-form" action="?insert=true" method="post"><!---../api.php?action=insert&insert=true-->
        <!---hidden ID field--------------- action="../api.php?action=insertItem"-->
        <input id="id" name="id" type="hidden"/><br> 

        <!--------------->
        <img id="logo-display" src="./uploads/default.png" alt="">
        
            <h2 id="item-title">new link</h2><!--otherwise it gets overwritten--->

        <div class="child w50 form-item">
            <div class="w70">
            <h3>name*</h3>
            <input id="name" placeholder="name" type="text" name = "name"/><br>
            </div>
        </div>

        <div class="child w50 form-item">
            <div class="w70">
            <h3>url</h3>
            <input id="url" placeholder="url" type="text" name = "url"  /><br>
            </div>
        </div>


    
        <div class="child w50 form-item">
            <div class="w70">
                <h3>logo</h3>
                <img id="logo-preview" src="./uploads/default.png" alt="">
                <p id="logo-info">logo info</p>
                <i id="remove-img" class="fas fa-times"></i>
                <input id="ifnewlogo" value="yes" type="hidden" /> 
                <input id="logo" type="file" name="fileupload" /> 
            </div>
        </div>
        

        <div class="child w50 form-item">
            <div class="w70">
            <h3>description</h3>
            <input id="description" placeholder="description" type="text" name = "description"/><br>
            </div>
        </div>

        <div class="child w50 form-item">
            <div class="w70">
            <h3>type*</h3>
            <select tabindex="1" id="type" name="type" >
                <option value="1">guideline</option>
                <option value="2">Helpfull Link</option>
                <option value="3">project</option>
                <option value="4">type</option>
            </select>          
            </div>
        </div>
    
        
        <div class="child w50 form-item">
            <div class="w70">
                <h3>status*</h3>
                <select tabindex="1" id="status" name="status" >
                    <option value="0">inactive</option>
                    <option value="1">active</option>
                </select>
            </div>

        </div>

        <div class="child special-submit w50 form-item">
            <div class="w70">

            <input class="submit-button" id="save_or_edit" value="submit" type="submit" />
            </div>
        </div>



        </div> 

    </form>
    <div>
