<?php
include './data/database_connection.php';
include './data/functions.php';


    $database = OpenCon();

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        $_SESSION['wrong_pw_times'] = 0;
        $_SESSION['blocked'] = false;
    } else {
        if ($_SESSION['blocked'] == 'yes') {
            //header("Location: blocked.php");
        }
    }

    if (!isset($_SESSION['blocked'])){
        $_SESSION['blocked'] = 'no';
    }

    if ($_SESSION['loggedin'] == 'yes'){
        header("Location: index.php");
    } 

  
     ?>
  
    <html>
   
        <head>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			
			<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
			<link rel="icon" href="/favicon.ico" type="image/x-icon">
			
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="./css/basic-settings.css">
            <link rel="stylesheet" href="./css/flexbox.css">
            <link rel="stylesheet" href="./css/header.css">

            <link rel="stylesheet" href="./css/form.css">
            <link rel="stylesheet" href="./css/login.css">
            <link rel="stylesheet" href="./css/mobile.css">

			
			 <!----sweet alert -> plugin for nice ---->
        	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>
			
			<!----jquery ---->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
			
            <script src="./js/script.js"></script>

        </head>
  
<body>
	   
      <!--------------LOGIN SECTION------------------->
      <div class="content-area parent">
            <form id="login" action="?login=true" method="post">
                <h2>first, we need your password</h2>
                        <div class="child form-item">
                            <input tabindex="1" placeholder="password" type="password" name = "password_input" required/><br/>
                        </div>

                        <div class="child form-item">
                            <input tabindex="2" class="submit-button" type="submit" value="login" name="submit">
                        </div>
            </form>
      </div>
        <!---------------------------------->
</body>

</html>
    <?php
   
    function hash_password($password_enterd,$options){
            $hashed_password = password_hash($password_enterd, PASSWORD_BCRYPT,$options); // PASSWORD_DEFAULT = ALGORYTHM
            return $hashed_password;
    }
    

    function check_password($user_input, $database){
        
        $select_statement = "SELECT value FROM config WHERE name='password'";

        if ($result = $database->query($select_statement)) {
            while ($row = $result->fetch_assoc()) {
                $password_from_db = $row["value"];
            }
        }
        return password_verify($user_input, $password_from_db);
    }

    function login($password_correct){
        if ($password_correct == 'yes'){
            $_SESSION['loggedin'] = 'yes'; 
            return 'yes';
        } else {         
		echo "<script type='text/javascript'>
    
		  Swal.fire({
			  position: 'top-end',
			  icon: 'error',
			  title: 'wrong password!',
			  showConfirmButton: false,
			  timer: 1500
          })
          
          </script>";  
            $_SESSION['wrong_pw_times'] += 1;

            return 'no';
            if ($_SESSION["wrong_pw_times"] >= 3){
                $_SESSION['blocked'] = 'yes';
            }
        }
    }

    
    if (isset($_GET['login'])) {
        $hashed_password = hash_password( $_POST["password_input"],$options);
        $password_correct = check_password($_POST["password_input"], $database);
        $_SESSION["loggedin"] = login($password_correct); // works - saves 1 in session variable
        if ($_SESSION["loggedin"] == 'yes'){
            header ("Location: index.php");
        } 
    }
?>