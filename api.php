
<?php

include './data/validations.php';
include './data/database_connection.php';

    
//globale definitonen
define('MYSQL_HOST', 'localhost');
define('MYSQL_USER', 'root');
define('MYSQL_PW', '');
define('MYSQL_DB', '70218_digicubedev');

checkDB();
    

function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output." (api.php)", JSON_HEX_TAG) . 
');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}

$con = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PW,MYSQL_DB);    
$database = $con;

$action = @$_GET['action'];
$type = @$_GET['type'];

if(!$action){
    $action = 'getData';        
}


switch ($action){
    case 'getData':
        if($_GET['type'] == 'item'){
            $tablename = 'link';
        } else {
            $tablename = 'item';
        }
       
        $sql = "SELECT * FROM $tablename";
        $results = $con->query($sql);
        $i=0;

        //array instanzieren
        $array = array();
        while($res = mysqli_fetch_array($results)){

            foreach($res as $key => $val){
                $array['data'][$i][$key] = $val;
            }
            $i++;
        }
        $array['success'] = array();
        $array['error'] = array();
        $array['sql'] = $sql;

        //json zurrückgeben
        echo json_encode($array);
    
        break;


    case 'getData_of_row':
        
        if($_GET['type'] == 'item'){
            $tablename = 'link';
        } else {
            $tablename = 'item';
        }

        $id = $_GET['id'];
        $sql = "SELECT * FROM $tablename WHERE id=$id";
        $results = $con->query($sql);
        $i=0;
        $array = array();
        while($res = mysqli_fetch_array($results)){
            foreach($res as $key => $val){
                $array['data'][$i][$key] = $val;
            }
            $i++;
        }
        $array['success'] = array();
        $array['error'] = array();
        $array['sql'] = $sql;
        echo json_encode($array);
    
        break;

        case 'update':       
            $ID = $_POST['id'];         
            $name = $_POST['name'];
            $url = $_POST['url'];
            $project_ID = $_POST['project'];
            $type_ID = $_POST['type'];
            $status = $_POST['status'];

            /** ----------------remove unusefull things------------------- */
            $ID = remove_specials($ID,$database);
            $name = remove_specials($name,$database);
            $url = remove_specials($url,$database);
            $project_ID = remove_specials($project_ID,$database);
            $type_ID = remove_specials($type_ID,$database);
            $status = remove_specials($status,$database);

            /**------------------------------------------------------------- */

            /** ----------------backend validation------------------- */
            $ID_OK = check_number($ID,12);
            $name_OK = check_textfield($name,255);
            $url_OK = check_textfield($url,255);
            $project_ID_OK = check_number($type_ID,12);
            $type_ID_OK = check_number($type_ID,12);
            $status_OK = check_number($status,12);

            /**-------------------------------------------------------------*/

            if ($ID_OK && $name_OK && $url_OK && $project_ID_OK && $type_ID_OK && $status_OK == true){
            /**---------------------------------------------------------------- */

                $timestamp = date('Y-m-d H:i:s');
                $sql = "UPDATE link SET name= '$name', url= '$url', project_ID=$project_ID, type_ID=$type_ID, status=$status WHERE ID = $ID;";
                echo $sql;

                $con->query($sql);

    
                $array = array();
                $array['success'] = array();
                $array['error'] = array();
                $array['sql'] = $sql;
                echo json_encode($array);
            } 
            break;


break;
    case 'insert':
        $name = $_POST['name'];
        $url = $_POST['url'];
        $project_ID = $_POST['project'];
        $type_ID = $_POST['type'];
        $status = $_POST['status'];

    
        /** ----------------remove unusefull things-------------------*/
        $name_final = remove_specials($name,$database);
        $url_final = remove_specials($url,$database);
        $project_final = remove_specials($project_ID,$database);
        $type_final = remove_specials($type_ID,$database);
        $status_final = remove_specials($status,$database);
        /**------------------------------------------------------------- */

        /** ----------------backend validation-----------------
        $name_OK = check_textfield($name_final,255);
        $url_OK = check_textfield($url_final,255);
        $project_ID_OK = check_number($project_final,12);
        $type_ID_OK = check_number($type_final,12);
        $status_OK = check_textfield($status_final,12);

        /**-------------------------------------------------------------
        
        if ($name_OK && $url_OK && $project_ID_OK && $type_ID_OK && $status_OK == true){
        /**---------------------------------------------------------------- */

            $timestamp = date('Y-m-d H:i:s');

            $sql = "INSERT INTO `link` (`ID`, `name`, `url`, `project_ID`, `type_ID`, `status`,`created`) VALUES (NULL, '$name_final', '$url_final', $project_final, $type_final, '$status_final','$timestamp');";

            $con->query($sql);

            $array['success'] = array();
            $array['error'] = array();
            $array['sql'] = $sql;
            echo json_encode($array);
       /**-- }  */
        break;
    
        case 'delete':
            $id = $_GET['id'];

            if($_GET['type'] == 'item'){
                $tablename = 'link';
            } else {
                $tablename = 'item';
            }
            
            $sql = "DELETE FROM $tablename WHERE ID=$id";
            echo $sql;
            $con->query($sql);
            $array = array();
            $array['success'] = array();
            $array['error'] = array();
            $array['sql'] = $sql;
            echo json_encode($array);

        break;

        case 'insertItem':
            $logo = $_POST['logo'];
            $name = $_POST['name'];
            $type = $_POST['type'];
            $url = $_POST['url'];
            $description = $_POST['description'];
            $status = $_POST['status'];

            $timestamp = date('Y-m-d H:i:s');

            $sql = "INSERT INTO item (`ID`, `name`, `type`, `url`, `description`, `logo`,`status`,`created`) VALUES (NULL, '$name', $type, '$url', '$description', '$logo',$status,'$timestamp');";

            $con->query($sql);

            $array['success'] = array();
            $array['error'] = array();
            $array['sql'] = $sql;
            echo json_encode($array);
            break;

        
            case 'updateItem':
                $ID = $_POST['id'];
                $name = $_POST['name'];
                $type = $_POST['type'];
                $url = $_POST['url'];
                $logo = $_POST['logo'];
                $description = $_POST['description'];
                $status = $_POST['status'];

    
                $timestamp = date('Y-m-d H:i:s');

                $sql = "UPDATE item SET name='$name', type=$type, url='$url', description='$description', logo='$logo',status=$status,updated='$timestamp' WHERE ID=$ID;";

                /**
                 * UPDATE item SET name='$name', type=$type, url='$url', description='$description', logo='$logo',status=$status,updated='$timestamp' WHERE ID=$ID;
                 * 
                 * 
                 */
                $con->query($sql);
    
                $array['success'] = array();
                $array['error'] = array();
                $array['sql'] = $sql;
                echo json_encode($array);
                break;
            
                
            case 'delete':
                $id = $_GET['id'];

                if($_GET['type'] == 'item'){
                    $tablename = 'link';
                } else {
                    $tablename = 'item';
                }
                
                $sql = "DELETE FROM $tablename WHERE ID=$id";
                echo $sql;
                $con->query($sql);
                $array = array();
                $array['success'] = array();
                $array['error'] = array();
                $array['sql'] = $sql;
                echo json_encode($array);

            break;

        
        case 'changeStatus':
            $id = $_POST['ID'];
            $old_status = $_POST['old_status'];
            $type = $_GET['type'];


            if($_GET['type'] == 'item'){
                $tablename = 'link';
            } else {
                $tablename = 'item';
            }
       
            
            if($old_status == 0){
                $new_status = 1;
            } else {
                $new_status = 0;
            }
            $sql = "UPDATE $tablename SET status= $new_status WHERE ID=$id";
            echo $sql;
            $con->query($sql);
            $array = array();
            $array['success'] = array();
            $array['error'] = array();
            $array['sql'] = $sql;
            echo json_encode($array);

            break;
        
        case 'getID':
            $id = $_GET['ID'];

            $sql = "SELECT name FROM item WHERE ID=$id";
            $result = $database->query($sql);
            while ($row = $result->fetch_assoc()) {
                $name = $row['name'];
            }
            echo $name;
            
            break;
}



function checkDB(){    
if ($con = mysqli_connect(MYSQL_HOST,MYSQL_USER,MYSQL_PW)){
    if ($con -> select_db(MYSQL_DB)){
    } else {

    }   
}
}?>