<?php
include './data/database_connection.php';
include './data/functions.php';

    
    $database = OpenCon();

    if(!isset($_GET['show_item'])){
        $_GET['show_item'] = NULL;
    
    } 
    
    if(!isset($_GET['delete'])){
        $_GET['delete'] = NULL;
    
    } 

    if(!isset($_GET['pick'])){
        $_GET['pick'] = NULL;
    
    } 
    
    if(!isset($_GET['change'])){
        $_GET['change'] = NULL;
    
    } 

    if (session_status() == PHP_SESSION_NONE) {
        session_start();
        $_SESSION['wrong_pw_times'] = 0;
        echo "new session";
    
    }else {
        if($_SESSION['blocked'] == 'yes') {
        header("Location: blocked.php");
        } else if ($_SESSION['blocked'] == 'no'){
        }

    }
    if ($_SESSION['loggedin'] == 'yes'){
        //header("Location: index.php");
    
    } else if ($_SESSION['loggedin'] == 'no'){
        header("Location: login.php");
    
    }
   
    ?>
      n for nice ---->
             <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>

        </head>


     <!--------------nav------------------->
			<div id="myNav" class="overlay">
			  <a href="javascript:void(0)" class="closebtn closeNav" >&times;</a>
			  <div class="overlay-content">
				<a href="index.php">Home</a>
				<a href="add.php">Add</a>
				<a href="modify.php">Modify</a>
				<a id="logout" href="login.php?logout=true">Logout</a>
			  </div>
			</div>
		
			<span class="hamburger-icon openNav" >&#9776;</span>


            
    
    <h2>What would you like to modify?</h2>

      <!--------------OPTIONS------------------->
  <head>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">

            <!-- jquery-->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

			<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
			<link rel="icon" href="/favicon.ico" type="image/x-icon">
			
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="./css/basic-settings.css">
            <link rel="stylesheet" href="./css/flexbox.css">
            <link rel="stylesheet" href="./css/header.css">

            <link rel="stylesheet" href="./css/form.css">

            <script src="./js/script.js"></script>
            
             <!----sweet alert -> plugi
      <div class="parent">
            <a class="child option-area-add" href="?show_item=item">
                    <h3>Item</h3>
            </a>
            <a class="child option-area-add" href="?show_item=project">
                    <h3>Project</h3>
            </a>
            <a class="child option-area-add" href="?show_item=type">
                    <h3>Type</h3>
            </a>
            <a class="child option-area-add" href="?show_item=guideline">
                    <h3>Guideline</h3>
            </a>
            <a class="child option-area-add" href="?show_item=helpfull_link">
                    <h3>Helpfull link</h3>
            </a>

         
        </div>
        <!---------------------------------->

<?php

    echo "<div class='add-section'>";
    if ($_GET['show_item'] == 'item'){
        ?>
        <div class="parent modify-option">
            <form action="?pick=true&type=item"  method="post">
                <?php get_options(NULL,"item",$database);?>
                <div class="child form-item">
                <input class="submit-button" type="submit" value="pick">
            </div>     
        </form>
       </div>
        <?php

    } 

    if ($_GET['show_item'] == 'project'){
        ?>
        <div class="parent modify-option">
            <form action="?pick=true&type=project"  method="post">
            <?php get_options(NULL,$_GET['show_item'],$database);?>
            <div class="child form-item">
                <input class="submit-button" type="submit" value="pick">
            </div>        
            </form>
        </div>
        <?php

    } 

    
    if ($_GET['show_item'] == 'type'){
        ?>
        <div class="parent modify-option">
            <form action="?pick=true&type=type"  method="post">
            <?php get_options(NULL,$_GET['show_item'],$database);?>
            <div class="child form-item">
                <input class="submit-button" type="submit" value="pick">
            </div>
            </form>
        </div>
        <?php

    } 

    if ($_GET['show_item'] == 'guideline'){
        ?>
        <div class="parent modify-option">
            <form action="?pick=true&type=guideline"  method="post">
            <?php get_options(NULL,$_GET['show_item'],$database);?>
            <div class="child form-item">
                <input class="submit-button" type="submit" value="pick">
            </div>
            </form>
        </div>
        <?php

    } 

    if ($_GET['show_item'] == 'helpfull_link'){
        ?>
        <div class="parent modify-option">
            <form action="?pick=true&type=helpfull_link"  method="post">
            <?php get_options(NULL,$_GET['show_item'],$database);?>

            <div class="child form-item">
                <input class="submit-button" type="submit" value="pick">
            </div>

            </form>
        </div>
        <?php
    } 

    if (isset($_GET['logout'])) {
        $_SESSION['loggedin'] = 'no';
        header("Location: login.php");
    }


    if ($_GET['pick'] == true) {

        echo '<div class="child modify-option form-item">';

        if ($_GET['type'] == 'item'){
            $id =  $_POST['item'];
            $type =  $_GET['type'];
            echo "<form id='search' action='?change=true&id=$id&type=$type' method='post'>";


            $id = $_POST['item'];
            get_options_active_or_not($id,NULL,'item',$database); // item = have to do it dynamically (with if -> to filter)
            echo "</form>";

        }
        
        if ($_GET['type'] == 'project'){

            $id =  $_POST['project'];
            $type =  $_GET['type'];
            echo "<form id='search' action='?change=true&id=$id&type=$type' method='post'>";

            $id = $_POST['project'];
            get_options_active_or_not($id,NULL,'project',$database);
            echo "</form>";


        }
        
        if ($_GET['type'] == 'type'){
            $id =  $_POST['type'];
            $type =  $_GET['type'];
            echo "<form id='search' action='?change=true&id=$id&type=$type' method='post'>";

            $id = $_POST['type'];
            get_options_active_or_not($id,NULL,'type',$database);
            echo "</form>";

        }
        
        if ($_GET['type'] == 'guideline'){

            $id =  $_POST['guideline'];
            $type =  $_GET['type'];
            echo "<form id='search' action='?change=true&id=$id&type=$type' method='post'>";

            $id = $_POST['guideline'];
            get_options_active_or_not($id,NULL,'guideline',$database);
            echo "</form>";


        }
        
        if ($_GET['type'] == 'helpfull_link'){

            $id =  $_POST['helpfull_link'];
            $type =  $_GET['type'];
            echo "<form id='search' action='?change=true&id=$id&type=$type' method='post'>";

            $id = $_POST['helpfull_link'];
            get_options_active_or_not($id,NULL,'helpfull_link',$database);
            echo "</form>";


        }


        echo '</div>';
    
    }
  
    if ($_GET['change'] == true) {
        $id = $_GET['id'];
        $option = $_POST['option'];
        $type = $_GET['type'];

        change_item($option,$type,$id,$database);
    }


    echo '</div>';


?>

