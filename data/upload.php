<!--
<form action="upload.php" method="post" enctype="multipart/form-data">
  Select image to upload:
  <input type="file" name="fileToUpload" id="fileToUpload">
  <input type="submit" value="Upload Image" name="submit">
</form>
-->

<?php

// Check if image file is a actual image or fake image


// Check if file already exists

function check_file_exists($target_file){
    if (file_exists($target_file)) {
      /*not sucessfull */
      echo "<script type='text/javascript'>
    
      Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Sorry, $target_file already exists. We picked the file wich was already uploaded!',
          showConfirmButton: false,
          timer: 1500
          })
          
          </script>";  

    $uploadOk = 0;
    } else {
        $uploadOk = 1;
    }

    return $uploadOk;

}

function check_file_size($file_size){
    if ($file_size > 10) { //not bigger then 10mb
            
            /*not sucessfull */
            echo "<script type='text/javascript'>
    
            Swal.fire({
                  position: 'top-end',
                  icon: 'success',
                  title: 'your file $target_file! is to big',
                  showConfirmButton: false,
                  timer: 1500
              })
          
              </script>";          
              $uploadOk = 0;
      } else {
          $uploadOk = 1;
      }

      return $uploadOk;
}

function check_file_format($imageFileType){
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
      echo "<br>Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
    } else {
        $uploadOk = 1;
    }

    return $uploadOk;
    
}

function finish_file($uploadOk, $target_file){
    $error = false;

    foreach ($uploadOk as $i => $element) {
        if ($uploadOk[$i] == 0){
        $error = true;
        }
    }
        
    if ($error == true) {
            /*not sucessfull */
            echo "<script type='text/javascript'>
    
            Swal.fire({
                  position: 'top-end',
                  icon: 'success',
                  title: 'there was a error with uploading your file $target_file!',
                  showConfirmButton: false,
                  timer: 1500
              })
          
              </script>";  
            
    } else {
        move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);

        /*uploaded file sucessfully message */
        echo "<script type='text/javascript'>
    
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'your file $target_file was uploaded sucessfully',
            showConfirmButton: false,
            timer: 1500
          })
    
        </script>";        
      }
    }


    function check_file_if_image($file_tmpname,$param_check_image_size){

        $check = getimagesize($file_tmpname);
    
        if($check !== false) {
          $uploadOk = 1;
        } else {

        /*uploaded file sucessfully message */
        echo "<script type='text/javascript'>
    
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'your file $target_file is not a image',
            showConfirmButton: false,
            timer: 1500
          })
    
        </script>"; 
        $uploadOk = 0;
        }
        return $uploadOk;
      }





function upload_file($file_name,$file_tmpname,$file_size){
    /**--------------------constants for upload file function -------------------*/
    $target_dir = "uploads/"; // . = aktueles verzeichnise ->  ./
    //ausgeben wo ich aktuell bin
    $target_file = $target_dir . basename($file_name);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    /***------------------------------------------------------------------- */
    
    $param_check_image_size = $file_tmpname;
    
    $file_if_image_OK = check_file_if_image($file_tmpname,$param_check_image_size);
    
    $file_exists_OK = check_file_exists($target_file);

    $file_size = $file_size / 1000000; // = byte -> mb
    $file_size_OK = check_file_size($file_size);
    
    
    $file_format_OK = check_file_format($imageFileType);
    
    $uploadOk = array(
        $file_exists_OK,
        $file_size_OK,
        $file_format_OK,
        $file_if_image_OK,
    );
    
    finish_file($uploadOk,$target_file);
}

?>