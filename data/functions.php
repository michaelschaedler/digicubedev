<?php

include 'validations.php'; 
if (session_status() == PHP_SESSION_NONE) {
    session_start();
    $_SESSION['wrong_pw_times'] = 0;

}else {
    if($_SESSION['blocked'] == 'yes') {
    echo "you are blocked";
    //header("Location: blocked.php");
    } else if ($_SESSION['blocked'] == 'no'){
        echo "<br>you are not blocked<br>";
    }
    
    
    if(!isset($_SESSION['loggedin'])){
    echo "no value for loggedin";
    }  
}


/**
 * 
 * @name: getfilter
 * @param: database
 * @return: NULL
 * 
 * returns options for html field
 *
 */

function getfilter($database){

    echo "
    <div class='filter-section'>
        <form  class='filter-form' action='?filter=true' method='post'>
            <div class='filter'>";
            echo getFilterOptions(1,1,'project',$database);
            echo "<i tabindex='1' id='remove-projectfilter-button' class='fas fa-window-close'></i>";
            echo "</div>
            <div class='filter'>";
            echo getFilterOptions(2,1,'type',$database);
            echo "<i tabindex='1' id='remove-typefilter-button' class='fas fa-window-close'></i>";
            echo "</div>
            </form>
    </div>";
}

/**
 * 
 * @name: get_name
 * @param: type, id_to_search, database
 * @return: name_result
 * 
 * returns the name of the id 
 *
 */


function get_name($type,$id_to_search, $database){
    $select_statement = "SELECT * FROM item;";

    $result = $database->query($select_statement);

    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $id_result = $row["ID"];
        $name_result = $row["name"];

        if ($id_result == $id_to_search){
          return $name_result;
        }
      }
    } 
 }


 /**
 * 
 * @name: get_options
 * @param: filter,type,database 
 * returns options dropdown
 *
 */
function get_options($filter,$type,$database){
  
    //project == type 3
    //type == type 4
    
    if ($type == "item"){
        $select_statement = "SELECT * FROM link";
    } else {
        if ($type == "type"){
            $type_nr = 4;
        } else if ($type == "project"){
            $type_nr = 3;
        } else if ($type == "helpfull_link"){
            $type_nr = 2;
        } else if ($type == "guideline"){
            $type_nr = 1;
        } 
        $select_statement = "SELECT * FROM item WHERE type = $type_nr";
    } 
 
    if ($result = $database->query($select_statement)) {
        echo '<div class="child form-item">';
        echo "<select id='item_changer' name='$type'>";
        echo "<option class='default' value='' disabled selected hidden>please pick an option</option>";

        if ($filter){
            echo "<option value='0' label='text'>no Filter</option>";
        }

        $total_rows = 0;
        while ($row = $result->fetch_assoc()) {
                    $total_rows++;
                      //unset($id, $name);
                      $id = $row['ID'];
                      $status = $row['status'];
                      if ($status == 1){
                        $active_color = 'green';
                      } else{
                        $active_color = 'red';
                      }

                      $name = $row['name'];

                      if ($type == 'item' && $name == NULL ){
                        $name = $row['url'];

                    }
                      echo '<option class="'.$active_color.'" value="'.$id.'">'.$name.'</option>';

    }
    
    echo '</select>';
    echo '</div>';

    if ($total_rows == 0){
        echo "<br><br> there are no results found for the type <strong>$type</strong> <br><br>";
    }
    }
}


function getFilterOptions($tabindex,$filter,$type,$database){


    if ($type == "item"){
        $select_statement = "SELECT * FROM link";
    } else {
        if ($type == "type"){
            $select = "<select tabindex='$tabindex' class='filter-select type' id='item_changer' name='$type' required>";
            $please_pick = "<option class='default' value='' disabled selected hidden>type</option>";
            $type_nr = 4;
        } else if ($type == "project"){
            $select = "<select tabindex='$tabindex' class='filter-select project' id='item_changer' name='$type' required>";
            $please_pick = "<option class='default' value='' disabled selected hidden>project</option>";
            $type_nr = 3;
        } else if ($type == "helpfull_link"){
            $type_nr = 2;
        } else if ($type == "guideline"){
            $type_nr = 1;
        } 
        $select_statement = "SELECT * FROM item WHERE type = $type_nr";
    } 
 
    if ($result = $database->query($select_statement)) {
        echo $select;
        echo $please_pick;

        $total_rows = 0;
        while ($row = $result->fetch_assoc()) {
                    $total_rows++;

                      //unset($id, $name);
                      $id = $row['ID'];
                      $status = $row['status'];
                      if ($status == 1){


                        $name = $row['name'];

                        if ($type == 'item' && $name == NULL ){
                           $name = $row['url']; 
                        }
                         echo '<option class="default-options" value="'.$name.'">'.$name.'</option>';

                      } 
    }
    
    echo '</select>';

    if ($total_rows == 0){
        echo "<br><br> there are no results found for the type <strong>$type</strong> <br><br>";
    }
    }
}
/**
 * 
 * @name: get_options_only_active
 * @param: filter,type,database 
 * returns options dropdown
 *
 */
function get_options_only_active($tabindex,$filter,$type,$database){
  
    //project == type 3
    //type == type 4
    
    if ($type == "item"){
        $select_statement = "SELECT * FROM link";
    } else {
        if ($type == "type"){
            $type_nr = 4;
        } else if ($type == "project"){
            $type_nr = 3;
        } else if ($type == "helpfull_link"){
            $type_nr = 2;
        } else if ($type == "guideline"){
            $type_nr = 1;
        }
        
        echo $select_statement;
        $select_statement = "SELECT * FROM item WHERE type = $type_nr";
    } 
 
    if ($result = $database->query($select_statement)) {
        //echo "<select id='item_changer' name='$type' required>";
        //echo "<option class='default' value='' disabled selected hidden>please pick an option</option>";

        if ($filter){
            echo "<option value='0' label='text'>no Filter</option>";
            echo "<option value='test' label='text'>no testtest</option>";

        }

        $total_rows = 0;
        while ($row = $result->fetch_assoc()) {
                    $total_rows++;

                      //unset($id, $name);
                      $id = $row['ID'];
                      $status = $row['status'];
                      if ($status == 1){


                        $name = $row['name'];

                        if ($type == 'item' && $name == NULL ){
                           $name = $row['url']; 
                        }
                         echo '<option value="'.$id.'">'.$name.'</option>';
                      } 
    }
    
    echo '</select>';

    if ($total_rows == 0){
        echo "<br><br> there are no results found for the type <strong>$type</strong> <br><br>";
    }
    }
}


/**
 * 
 * @name: get_options_active_or_not
 * @param: id, filter, type, database
 * @return: options (if active or not)
 * 
 * returns options
 *
 */
function get_options_active_or_not($id,$filter,$type,$database){
    //project == type 3
    //type == type 4
    
    if ($type == "item"){
        $select_statement = "SELECT * FROM link WHERE ID = $id";
    } else if ($type == "type" || $type == "project" || $type == "helpfull_link" || $type == "guideline"){
        $select_statement = "SELECT * FROM item WHERE ID = $id";
    } else{
        echo "etwas ist schief gelaufen..";
    }

    if ($result = $database->query($select_statement)) {
        echo '<div class="child form-item">';
        echo "<select name=option>";
        echo "<option value='delete'>delete item</option>";
        while ($row = $result->fetch_assoc()) {
            $active = $row['status'];

            if ($active == 0){
                $name = "activate";
            } else  if ($active == 1){
                $name = "deactivate";
            } else {
                echo "etwas ist schiefgelaufen";
            }
            echo "<option value='$name'>$name</option>";
            echo "</select>";

            echo '<div class="child form-item">';
            echo '<input class="submit-button" type="submit" value="go">';
            echo "</div>";

         }

         echo "</div>";

}
}


/**
 * 
 * @name: send
 * @param: url,type,name_unconverted,database,project_ID, type_ID,description,logo
 * @return: 
 * 
    writes data into database
 */

function send($url,$type,$name,$database,$project_ID, $type_ID,$description,$logo){
    $name = remove_bullshit($name,$database);
    $tempstamp = date('Y-m-d H:i:s');

    /*-------------------------- VALIDATE EVERY INPUT------------------------- */
    /**remove special characters, html signs etc. */
    $name = remove_bullshit($name,$database);
    $url = remove_bullshit($url,$database);
    $type = remove_bullshit($type,$database);
    $project_ID = remove_bullshit($project_ID,$database);
    $type_ID = remove_bullshit($type_ID,$database);
    $description = remove_bullshit($description,$database);
    $logo = remove_bullshit($logo,$database);


    /*check if its a number / text & length for database */
    $url_OK = check_textfield($url,255);
    $type_OK = check_number($type_ID,255);
    $name_OK = check_textfield($name,255);
    $project_ID_OK = check_number($project_ID,255);
    $type_ID_OK = check_textfield($type_ID,255);
    $description_OK = check_textfield($description,255);
    $logo_OK = check_textfield($logo,255);
    /***--------------------------------------------------------------------- */

    /**send to database if its okay */
    if ($url_OK &&  $type_OK &&  $name_OK &&  $name_OK &&  $project_ID_OK &&  $type_ID_OK &&  $type_ID_OK && $description_OK &&  $logo_OK == true){
        if ($type == 'type'){
            $sql_statement_insert = "INSERT INTO item (ID,name,type,description,status,created)
            VALUES ('','{$name}',4,'{$description}',1,'$tempstamp')";
        
        } else if($type == 'project'){
            $sql_statement_insert = "INSERT INTO item (ID,name,type,description,status,created,logo)
            VALUES ('','{$name}',3,'{$description}',1,'$tempstamp','{$logo}')";
    
        }  else if($type == 'guideline') {
            $sql_statement_insert = "INSERT INTO item (ID,name,type,description,status,created,logo,url)
            VALUES ('','{$name}',1,'{$description}',1,'$tempstamp','{$logo}','{$url}')";
    
        }  else if($type == 'helpfull_link') {
            $sql_statement_insert = "INSERT INTO item (ID,name,type,description,status,created,logo,url)
            VALUES ('','{$name}',2,'{$description}',1,'$tempstamp','{$logo}','{$url}')";
    
    
        }else if($type == 'item') {
            $sql_statement_insert = "INSERT INTO link (ID,name,url,project_ID,type_ID,created,status)    
            VALUES ('','{$name}','{$url}',$project_ID,$type_ID,'$tempstamp',1)";
        }
        
        $database->query($sql_statement_insert);
        echo "<script type='text/javascript'>
    
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: '$type was added sucessfull!',
            showConfirmButton: false,
            timer: 1500
          })
    
        </script>";
        
    } 
}


/**
 * 
 * @name: get_imagename_of_project
 * @param: database
 * @return: 
 * 
    writes data into database

    (only for items out of the link db table)
 */
function get_imagename_of_project($database,$project_ID){

    $select_statement = "SELECT * FROM item WHERE ID=$project_ID";

    if ($result = $database->query($select_statement)) {
        $row = mysqli_fetch_assoc($result);
        while($row){
            $logo = $row["logo"];
            $row = $result->fetch_assoc();
        }

        return $logo;
    }

}


/**
 * 
 * @name: show_item
 * @param: database
 * @return: 
 * 
    writes data into database

    (only for items out of the link db table)
 */
function show_item($database){
    $select_statement = "SELECT * FROM link";

    if ($result = $database->query($select_statement)) {
    $row = mysqli_fetch_assoc($result);

    echo '<div class="content-area parent">';

        while($row){
            $name = $row["name"];
            $url = $row["url"];
            $project_ID = $row["project_ID"];
            $type_ID = $row["type_ID"];
            $status = $row["status"];

            $project_name = get_name('project',$project_ID,$database);
            $type_name = get_name('type',$type_ID,$database);
                
            $row = $result->fetch_assoc();

            if ($name == null && $status==1){
                
                $image_name = get_imagename_of_project($database,$project_ID);
                $uploads_path = './uploads/';
                $image_url = $uploads_path.$image_name;

                echo "<a target='_blank' class='child workspace-item' href='$url'>";
                //echo "<div class='image-container'>";
                echo "<img href='$url' src='$image_url'>";
                echo "<h2 class='project'>$project_name</h2>";
                echo "<p class='type'>$type_name</p>";
                echo "</a>";

            } else if ($status == 1){

                $image_name = get_imagename_of_project($database,$project_ID);
                $uploads_path = './uploads/';
                $image_url = $uploads_path.$image_name;
                
                echo "<a target='_blank' class='child workspace-item' href='$url'>";
                echo "<img href='$url' src='$image_url'>";
                echo "<h2  class='name'>$name</h2>";
                echo "<p class='project'>$project_name</p> <p class='type'>$type_name</p>";
                echo "</a>";
            }
        }
    echo "</div>";
        }
}

/**
 * 
 * @name: show
 * @param: database
 * @return: 
 * 
 */
function show($type,$database){
    $select_statement = "SELECT * FROM item";
    
    if ($result = $database->query($select_statement)) {
    echo "<table>";    
            
    while($row = mysqli_fetch_assoc($result)){
        $name = $row["name"];

        echo "<tr>";
        echo "<td>$name <a href='delete.php?delete=true&name=$name&type=$type'>DELETE</a>";

        echo "</tr>";
    }
    echo "</table>";
    
    }
}


/**
 * 
 * @name: show_additional_info
 * @param: type_nr, database
 * @return: 
 * 
 */

function show_additional_info($type_nr,$database){
    $select_statement = "SELECT * FROM item WHERE type =$type_nr AND status=1";
    
    if ($result = $database->query($select_statement)) {
    echo "<div class='parent conten-area'>";

    while($row = mysqli_fetch_assoc($result)){
        $name = $row["name"];
        $url = $row["url"];
        $type_db = $row["type"];
        $description = $row["description"];
        $logo_path = $row["logo"];


        
        if ($logo_path == NULL){
            $logo_path = 'default.png';
        }

        echo "<a href='$url' target='_blank' class='child additional-item'>";
        echo "<h2>$name</h2>";
        echo "<img src='./uploads/$logo_path'>";
        echo "<p class='further-info'>$description</p>";
        echo "</a>";

    }
    echo "</div>";
    
    }
}
/**
 * 
 * @name: filter_one_category
 * @param: type,id,database
 * @return: 
 * 
 */

function filter_one_category($type,$id,$database){

    $select_statement = "SELECT * FROM link WHERE ".$type."_ID = $id;";

    if ($result = $database->query($select_statement)) {
        echo "<div class='show-items-container'>";
    
    $row = mysqli_fetch_assoc($result);
        
    echo '<div class="content-area parent">';
    
    while($row){
        $name = $row["name"];
        $url = $row["url"];
        $project_ID = $row["project_ID"];
        $type_ID = $row["type_ID"];
        $status = $row["status"];

        $project_name = get_name('project',$project_ID,$database);
        $type_name = get_name('type',$type_ID,$database);
            
        $row = $result->fetch_assoc();

        if ($name == null && $status==1){
            
            $image_name = get_imagename_of_project($database,$project_ID);
            $uploads_path = './uploads/';
            $image_url = $uploads_path.$image_name;

            echo "<a target='_blank' class='child workspace-item' href='$url'>";
            //echo "<div class='image-container'>";
            echo "<img href='$url' src='$image_url'>";
            echo "<h2>$project_name</h2>";
            echo "<p class='further-info'>$type_name</p>";
            echo "</a>";

        } else if ($status == 1){

            $image_name = get_imagename_of_project($database,$project_ID);
            $uploads_path = './uploads/';
            $image_url = $uploads_path.$image_name;
            
            echo "<a target='_blank' class='child workspace-item' href='$url'>";
            echo "<img href='$url' src='$image_url'>";
            echo "<h2>$name</h2>";
            echo "<p class='further-info'>$project_name II $type_name</p><p class='further-info'></p>";
            echo "</a>";
        }
    }
echo "</div>";
    
    }
}


/**
 * 
 * @name: filter_both_categories
 * @param: type_ID, project_ID, database
 * @return: 
 * 
 */
function filter_both_categories($type_ID,$project_ID,$database){

    $select_statement = "SELECT * FROM link WHERE project_ID = $project_ID AND type_ID = $type_ID;";
        
    if ($result = $database->query($select_statement)) {

        $row = mysqli_fetch_assoc($result);

        
        echo '<div class="content-area parent">';
    
    while($row){
        $name = $row["name"];
        $url = $row["url"];
        $project_ID = $row["project_ID"];
        $type_ID = $row["type_ID"];
        $status = $row["status"];

        $project_name = get_name('project',$project_ID,$database);
        $type_name = get_name('type',$type_ID,$database);
            
        $row = $result->fetch_assoc();

        if ($name == null && $status==1){
            
            $image_name = get_imagename_of_project($database,$project_ID);
            $uploads_path = './uploads/';
            $image_url = $uploads_path.$image_name;

            echo "<a target='_blank' class='child workspace-item' href='$url'>";
            //echo "<div class='image-container'>";
            echo "<img href='$url' src='$image_url'>";
            echo "<h2>$project_name</h2>";
            echo "<p class='further-info'>$type_name</p>";
            echo "</a>";

        } else if ($status == 1){

            $image_name = get_imagename_of_project($database,$project_ID);
            $uploads_path = './uploads/';
            $image_url = $uploads_path.$image_name;
            
            echo "<a target='_blank' class='child workspace-item' href='$url'>";
            echo "<img href='$url' src='$image_url'>";
            echo "<h2>$name</h2>";
            echo "<p class='further-info'>$project_name II $type_name</p><p class='further-info'></p>";
            echo "</a>";
        }
    }
echo "</div>";
    
    }
}




/**
 * 
 * @name: search
 * @param: database, input
 * @return: 
 * 
 */


function search($database,$input){    

    $sql_statement = "SELECT * FROM search_view 
    WHERE type LIKE '%$input%' OR url LIKE '%$input%' OR item 
    LIKE '%$input%' OR project LIKE '%$input%'";

    if($result=$database->query($sql_statement)){
    echo "<div class='show-items-container'>";
        
        while ($row = $result->fetch_assoc()) {
        $name = $row["item"];
        $url = $row["url"];
        $project_name = $row["project"];
        $type_name = $row["type"];


        if ($name == null){
            echo "<a target='_blank' href='$url'>";
            echo "<div class='item without-name'>";
            echo "<h5>$project_name</h5>";
            echo "<p>$type_name</p>";
            echo "</div>";
            echo "</a>";


        } else{
            echo "<a target='_blank' href='$url'>";
            echo "<div class='item with-name'>";
            echo "<h5>$name</h5>";
            echo "<p>$project_name &nbsp; &nbsp; $type_name</p>";
            echo "</div>";
            echo "</a>";

        }
        }
        echo "</div>";
    }
        
     //-----------------------------------------------------------------------------
 
 }

 
/**
 * 
 * @name: get_ID
 * @param: type, name_to_search, database
 * @return: 
 * 
 */
function get_ID($type,$name_to_search, $database){
    $select_statement = "SELECT * FROM $type;";
    echo $select_statement;
    echo "<br>";
    echo $name_to_search;
    echo "<br>";

    $result = $database->query($select_statement);

      while($row = $result->fetch_assoc()) {
        $id = $row["ID"];
        $name_result = $row["name"];
        echo $name_result;

        if ($name_result == $name_to_search){
          return $id;
        }
      }
 }


 /**
 * 
 * @name: go_back_home
 * @param: type,
 * @return: 
 * 
 * opens the table with the specific type
 * 
 */
function go_back_home($type){
  header("Location: http://localhost/digicube.dev/final/delete.php?show_item=$type");

}

 /**
 * 
 * @name: change_item
 * @param: change,type,id,database
 * @return: 
 * 
 * opens the table with the specific type
 * 
 */
function change_item($change,$type,$id,$database){
    /**select table */
    if ($type == "item"){
        $type_tablename = "link";
    } else {
        $type_tablename = "item";
    }

    /**select action */
    if ($change == 'delete'){
        $sql = "DELETE FROM $type_tablename WHERE ID=$id;";
    } else if($change == 'activate'){
        $sql = "UPDATE $type_tablename SET status = 1 WHERE ID=$id;";
    } else if($change = 'deactivate'){
        $sql = "UPDATE $type_tablename SET status = 0 WHERE ID=$id;";
    } 

    $database->query($sql);

    echo "<script type='text/javascript'>
    
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: '$type was $change'+'d'+' sucessfull!',
        showConfirmButton: false,
        timer: 1500
      })

    </script>";
    
    //go_back_home($type);

}
?>