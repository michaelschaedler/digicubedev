<?php

function remove_specials($text,$database){
    $text_without_html = htmlspecialchars($text);
    $final_text = trim($text_without_html);

    return $final_text;
} 

    //--------------textfield----------------
    function check_textfield($text, $laenge){
        if(strlen($text)<$laenge){
         $textfieldOK = true;
        } else {
        $textfieldOK = false;
        }

        return $textfieldOK;
    }

    //--------------number----------------
    function check_number($text, $laenge){
    
        if(strlen($text)< 1){
            $textfieldOK = true;//the field is empty -> the type doesnt need a number
        }
        else if(strlen($text)< $laenge){
            if (is_numeric($text)){
                $textfieldOK = true;
            } else{
                $textfieldOK = false;
            }
        } else {
            $textfieldOK = false;
        }
        return $textfieldOK;
    }

?>