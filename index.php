<!--------------------- PRESET PHP ----------------------------------->
<?php
include './data/database_connection.php';
include './data/functions.php';

$database = OpenCon();

if(!isset($_SESSION['loggedin'])){
    $_SESSION['loggedin'] = 'no';

} 

if(!isset($_SESSION['blocked'])){
    $_SESSION['blocked'] = 'no';

} 

if (session_status() == PHP_SESSION_NONE) {
    session_start();
    $_SESSION['wrong_pw_times'] = 0;
    $_SESSION['loggedin'] = 'no';


}else {
    if($_SESSION['blocked'] == 'yes') {
    //header("Location: blocked.php");
    } else if ($_SESSION['blocked'] == 'no'){
    } 
}

if (isset($_GET['logout'])) {
    $_SESSION['loggedin'] = 'no';
    header("Location: login.php");
}


if ($_SESSION['loggedin'] == 'yes'){

} else if ($_SESSION['loggedin'] == 'no'){
    header("Location: login.php");

}

if ($_SESSION['loggedin'] == 'yes'){

} else if ($_SESSION['loggedin'] == 'no'){
    header("Location: login.php");

}
?>
<!-------------------------------------------------------------------->
<html>
    <head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

    
        <!-- jquery-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
  

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="./css/basic-settings.css">
        <link rel="stylesheet" href="./css/flexbox.css">
        <link rel="stylesheet" href="./css/header.css">

        <link rel="stylesheet" href="./css/form.css">
        <link rel="stylesheet" href="./css/dashboard.css">
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">

        <!--font awesome--->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />

        <script src="./js/script.js"></script>


    </head>

    <body>



	      <!--------------nav------------------->
			<div id="myNav" class="overlay">
			  <a href="javascript:void(0)" class="closebtn closeNav" >&times;</a>
			  <div class="overlay-content">
              <img src="./uploads/default.png" alt="">
                <a href="index.php">Home</a>
                <a href="edit.php">Modify</a>
                <a id="logout" href="index.php?logout=true">Logout</a>>
			  </div>
			</div>
		
			<span class="hamburger-icon openNav" >&#9776;</span>

      <!--------------OPTIONS------------------->
      <div class="parent">
            <a class="child option-area" href="?show_option=true&option=workspace">
                    <h3>Workspaces</h3>
            </a>

            <a class="child option-area" href="?show_option=true&option=guideline">
                    <h3>Guidelines</h3>
            </a>

            <a class="child option-area" href="?show_option=true&option=helpfull_link">
                    <h3>Helpfull Links</h3>
            </a>
        </div>
        <!---------------------------------->
<?php
if (isset($_GET['show'])) {
    if (!isset($_GET['type'])){
    
    } else {
        if ($_GET['type'] == 'item'){
            show_item($_GET['type'],$database);        
        } else{
            show($_GET['type'],$database);        
        }   
    }
}

if (isset($_GET['filter'])) {
    getfilter($database);

    
    if(!isset($_POST['type'])){
        $_POST['type'] = 0;

    } 

    if(!isset($_POST['project'])){
        $_POST['project'] = 0;

    } 

    /**both filters selected */
    if ($_POST['project'] != 0 && $_POST['type'] != 0){
        filter_both_categories($_POST['type'],$_POST['project'],$database);

    } else if ($_POST['project'] != 0 || $_POST['type'] != 0){

        /*one project filter selected */
        if (($_POST['project'] != 0)){
            filter_one_category('project',$_POST['project'],$database);

        } else if ($_POST['type'] != 0) {
            filter_one_category('type',$_POST['type'],$database);
        }
    }else{

        header("Location: ?show=true&type=item");   

    }
}

if (isset($_GET['search'])) {
    search($database,($_POST['search_input']));
}



if (isset($_GET['show_option'])) {
    if ($_GET['option'] == 'workspace') {
        getfilter($database);        
        show_item($database);        
    }
    
    if ($_GET['option'] == 'guideline') {
        //guideline == type 1
        show_additional_info(1,$database);        
    }
    
    if ($_GET['option'] == 'helpfull_link') {
        //helpfull link == type 2
        show_additional_info(2,$database);        
    }
}?>


    </div>
</html>
    