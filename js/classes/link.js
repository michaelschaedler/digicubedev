


class link extends dataBaseObject {
    constructor(ID,name,url,project_ID,type_ID,status) {
      super();
      this.ID = ID;
      this.name = name;
      this.url = url;
      this.project_ID = project_ID;
      this.type_ID = type_ID;
      this.status = status;
      this.template = $('#project-template').html();
      this.type="item";
      this.finalLinks = [];
    }

    printInfo(){
      console.log("ID: " + this.ID);
      console.log("name: " + this.name);
      console.log("url: " + this.url);
      console.log("project_ID: " + this.project_ID);
      console.log("type_ID: " + this.type_ID);
      console.log("status: " + this.status);
    }
    removeDoubbledOptions(){
  
    }


    getFormSelectData(){

      $.ajax({
        url: "api.php?action=getData&type=",
        dataType: 'json',
        success: function(response) {
          response.data.forEach(function(row) {
              if (row.type == 3){
                $("#project").append($("<option />").val(row.ID).text(row.name));
              } else if (row.type == 4){
                $("#type").append($("<option />").val(row.ID).text(row.name));  
              }     
          });
        }
      });
    }  

    chooseAction(){
      if (this.ID == 0 ){
          return "insert";
      } else {
        return "update";
      }
    }

    checkRequierdFields(){
     if(this.url.length <= 0 || this.project_ID.length <= 0 || this.type_ID.length <= 0|| this.status.length <= 0){
      return false;
     } else{
       return true;
     }
    }
    saveData(){
      this.ID = $('#ID').val();
      this.name = $('#name').val() ;
      this.url = this.renderURL($('#url').val());
      this.project_ID = $('#project').val();
      this.type_ID = $('#type').val();
      this.status = $('#status').val();

    }
  
    renderData(){
      if (this.dataJSON.hasOwnProperty("data")){
        this.dataJSON.data.forEach(function(row) {
          if (row.logo){
            var logoWithPath = "./uploads/" + row.logo;
            row.logo = logoWithPath;
          } else {
            row.logo = "./uploads/default.png";
          }
        });

        this.dataJSON.data.forEach(function(row) {

          if (row.type == "1"){
            row.type = "guid";
          } else if (row.type == "2"){
            row.type = "hl";
          } else if (row.type == "3"){
            row.type = "pr";  
          } else if (row.type == "4"){
            row.type = "type";
          } else {
            row.type = "not found";
          }
      
    
          });
      } else{
        $('#load-additional').html("<h2>no data found, please add!</h2>");
      }
    }

    toDB(){
      this.ajax_call();
      closeForm('item');
      let obj = new item();
      showData(obj);
    }
    
    ajax_call(){
    //e.preventDefault();
    this.action = this.chooseAction();
    $.ajax({
        type: "post",
        url: "api.php?action="+this.action,
        data: {
            id: this.ID,
            name: this.name,
            url: this.url,
            project: this.project_ID,
            type: this.type_ID,
            status: this.status,
        },
        dataType: "json",
    });

    openTab('item');
    }


    openCorrectTab(){
      openTab('');
    }

    removeImage(){
      $('#logo-preview').hide();
      $('#remove-img').hide();
      $('#logo').show();
      $('#ifnewlogo').val('yes');
    }

    loadFormdata(id){
      $('#id').val(id);

      $.ajax({
          url: "api.php?action=getData_of_row&type=link&id=" + id,
          dataType: 'json',
          success: function(response) {
              $('#item-title').html(response['data'][0]['name']);
              $('#id').val(response['data'][0]['ID']);
              $('#name').val(response['data'][0]['name']);
              $('#url').val(response['data'][0]['url']);
              $('#description').val(response['data'][0]['description']);
              $('#logo-display').attr("src","./uploads/" + response['data'][0]['logo']);
              $('#status').val(response['data'][0]['status']);
              $('#type').val(response['data'][0]['type']);
              // label für input - Felder klasse active hinzufügen
              $('.labelset label').addClass('active');


              if (response['data'][0]['logo'] == 'default.png'){
                $('#logo-info').html('no logo found - add a new one!');
                $('#logo-preview').hide();
                $('#remove-img').hide();

              } else {
                $('#remove-img').show();
                $('#ifnewlogo').val('no');
                $('#logo').hide();
                $('#logo-info').hide();
                $('#logo-preview').attr("src","./uploads/" + response['data'][0]['logo']);
        
              }


          }
      });
    }
  }