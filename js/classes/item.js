
class item extends dataBaseObject{
    constructor(ID,name,url,description,logo,status) {
      super();
      this.ID = ID;
      this.name = name;
      this.url = url;
      this.description = description;
      this.logo = logo;
      this.status = status;
      this.template = $('#item-template').html();

      switch (this.type_ID){
        case 1:
        this.type = 'guideline';
        break;

        case 2:
        this.type = 'helpfull_link';
        break;

        case 3:
        this.type = 'project';
        break;

        case 4:
        this.type = 'type';
        break;

      }  
    }


    async uploadFile() {
      let formData = new FormData(); 

      formData.append("file", logo.files[0]);

      await fetch('upload.php', {
          method: "POST", 
          body: formData
      }); 
  }


    saveData(){
      this.url = this.renderURL($('#url').val());
      this.uploadFile();
      this.ID = $('#id').val();
      this.name = $('#name').val();
      this.description = $('#description').val();
      this.type = $('#type').val();
      this.newlogo = $('#ifnewlogo').val();

      if ( $('#ifnewlogo').val() == 'yes'){
        this.logo = $('#logo').val();
      } else {

      }
      this.logo = $('#logo').val();
      this.logo = this.logo.replace(/C:\\fakepath\\/i, '');
      this.status = $('#status').val();
    }
  
    printInfo(){
      console.log("ID: " + this.ID);
      console.log("name: " + this.name);
      console.log("url: " + this.url);
      console.log("description: " + this.description);
      console.log("logo: " + this.logo);
      console.log("status: " + this.status);
      console.log("type: " + this.type);
      console.log("new logo?: " + this.newlogo);
    }
  
    chooseAction(){
      if (this.ID == 0 ){
          return "insertItem";
      } else {
          return "updateItem";
      }
    }

    cutLogoPath(logoWithPath){
      var logoWithoutPath = logoWithPath.substring(logoWithPath.lastIndexOf("/"), logoWithPath.length);
      var finallogo = logoWithoutPath.substring(1);//remove slash
      return finallogo;
    }


    toDB(){
      if (!this.logo){
        this.logo="default.png";
      }

      if (this.newlogo == 'yes'){
        this.logo = this.logo;
      } else {
        this.logo = $('ifnewlogo').attr('src');
        this.logo = this.cutLogoPath($('#logo-preview').attr('src'));
      }
      this.ajax_call();
      closeForm('');

      let obj = new link();
      showData(obj);
    }



    checkRequierdFields(){
      if(this.name.length <= 0 ||this.ID.length <= 0 ||  this.status.length <= 0 || this.type.length <= 0 || this.newlogo.length <= 0){
       return false;
      } else{
        return true;
      }
     }

    ajax_call(){

      this.action = this.chooseAction();

      $.ajax({
          type: "post",
          url: "api.php?&action="+this.action,
          data: {
              id: this.ID,
              name: this.name,
              url: this.url,
              logo: this.logo,
              description: this.description,
              status: this.status,
              type:this.type,
          },
          dataType: "json",
      });
  
      openTab('');
      }

      
    openCorrectTab(){
      openTab('item');

    }
    


    loadFormdata(id){
      $('#id').val(id);
      $.ajax({
          url: "api.php?action=getData_of_row&type=item&id=" + id,
          dataType: 'json',
          success: function(response) {
              $('#item-title').html(response['data'][0]['url']);
              $('#ID').val(response['data'][0]['ID']);
              $('#name').val(response['data'][0]['name']);
              $('#url').val(response['data'][0]['url']);
              $('#project').val(response['data'][0]['project_ID']);
              $('#type').val(response['data'][0]['type_ID']);
              $('#status').val(response['data'][0]['status']);

              // label für input - Felder klasse active hinzufügen
              $('.labelset label').addClass('active');
          }
      });
    }  
  }