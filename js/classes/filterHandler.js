class filterHandler{
    
    filter(project,type){
      if  (type == null ){
          this.filterProject(project);
          this.displayRemoveFilterOption(true,false);
      } else if(project == null){
          this.filterType(type);
          this.displayRemoveFilterOption(false,true);
      } else {
          this.filterBoth(project,type);
          this.displayRemoveFilterOption(true,true);
      }
    }

    reset(){
        $( ".workspace-item" ).each(function() {
                $(this).show();
        });
    }


    displayRemoveFilterOption(ifProject, ifType){
        if (ifProject == true){
            $('#remove-projectfilter-button').show();
        } 
        if (ifType == true){
            $('#remove-typefilter-button').show();
        } 
    }

    RemoveRemoveFilterOption(ifProject, ifType){
            if (ifProject == true){
                $('#remove-projectfilter-button').hide();
                $('.project').prop('selectedIndex',0);
            }

            if (ifType == true){
                $('#remove-typefilter-button').hide();
                $('.type').prop('selectedIndex',0);
            }
    }



    filterBoth(project,type){
        console.log("filter both");
        this.filterProject(project);
        this.filterType(type);
    }

    removeOption(isRemoveType,isRemoveProject,filterAfterRemove){
        this.reset();
        console.log("remove option");
        console.log("isREmoveType: " + isRemoveType);
        console.log("isRemoveProject: " + isRemoveProject);
        console.log("filterAfterRemove: " + filterAfterRemove);

        if (isRemoveType == true){
            console.log("remove type");
            this.filterProject(filterAfterRemove);
            this.RemoveRemoveFilterOption(false,true);
        } else if (isRemoveProject == true){
            console.log("remove project");
            this.filterType(filterAfterRemove);
            this.RemoveRemoveFilterOption(true,false);
        }
    }


    filterType(name){
        $( ".workspace-item" ).each(function() {
            if ($(this).find('.type').text() != name){
                $(this).hide();
            } 
        });
    }

    filterProject(name){
        $( ".workspace-item" ).each(function() {
            if ($(this).find('.project').text() != name){
                $(this).hide();
            } 
        });
    }

}