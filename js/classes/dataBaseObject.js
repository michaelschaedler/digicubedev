class dataBaseObject {
    constructor() {
  
    }

    toDB(){
    }

    saveData(){
      
    }

    renderURL(url){
      if (url.length > 0){

        const https = 'https://';
        const http = 'http://'
        if (!url.includes(https,0) && !url.includes(http,0)){
            this.url = https + url;
        } else {
          this.url = url;
        } 
      } else {
        this.url = "";
      }

      return this.url;
    }
    
    removeImage(){

    }
  
    wichTab(){
      if ($('.wich-tab-info').val() == 'item'){
        return 'item';
      } else {
        return 'additional';
      }
    }

    createFittingObj(){
      if ($('.wich-tab-info').val() == 'item'){
        return new link();
      } else {
        return new item();
      }
    }

    getformularLink(){
      if ($('.wich-tab-info').val() == 'item'){
        return "sites/item_formular.php";
      } else {
        return "sites/link_formular.php";
      }
    }
    
    getformularID(){
      if ($('.wich-tab-info').val() == 'item'){
        return "#load";
      } else {
        return "#load-additional";
      }
    }
  
    getClassName(){
      if (this instanceof link){
          return "link";
      } else {
        return "item";
      }
    }

    closeForm(){
      $('#close-trigger').hide();
      $('#add-trigger').show();
      $('#load').hide();
    }
    
    changeStatus(){
      $.ajax({
          type: "post",
          url: "api.php?action=changeStatus&type=" +this.getClassName(),
          data: {
              ID: this.ID,
              old_status: this.status,
          },
          dataType: "json",
        
      });

      $('.1').html('<i class="fas fa-toggle-on"></i><input class="status-val" type="hidden" value="1">');
      $('.0').html('<i class="fas fa-toggle-off"></i><input class="status-val" type="hidden" value="0">');

      showData(this);
    }

    selectTemplate(){
      return this.template;
    }
  
    ID_to_name(){
    }
  
    delete(){
      if (confirm("Möchten Sie das Element wirklich löschen?")) {
        $.ajax({
            url: 'api.php?action=delete&id=' + this.ID +'&type=' + this.getClassName(),
            dataType: 'json',
        });
        this.openCorrectTab();
    }   
    }
  
    getFormSelectData(){

    }
    renderData(){
      if (this.dataJSON.hasOwnProperty("data")){
      this.dataJSON.data.forEach(function(row) {
        var projectname = ID_to_name(row.project_ID);
        row.project_ID = projectname;
      });
  
      
      this.dataJSON.data.forEach(function(row) {
        var typename = ID_to_name(row.type_ID);
        row.type_ID = typename;
      });
    } else {
      $('#load').html("<h2>no data found, please add!</h2>");
    }
    }
    
    fillData(){
      var html = Mustache.render(this.template, this.dataJSON);
      $('tbody').html(html);  
    }

    loadFormdata(){
      
    }
}
  