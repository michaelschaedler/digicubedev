$(function () { 
$('#remove-typefilter-button').hide();
$('#remove-projectfilter-button').hide();

  $( ".openNav" ).click(function() {
    document.getElementById("myNav").style.width = "100%";
  });

    $( ".closeNav" ).click(function() {
    document.getElementById("myNav").style.width = "0%";
  });
  $.getScript("js/classes/dataBaseObject.js");
  $.getScript("js/classes/link.js");
  $.getScript("js/classes/item.js");
  $.getScript("js/classes/filterHandler.js");
  $.getScript("js/classes/sorterHandler.js");
  $.getScript("js/liste.js");
  

  $("select").change(function() {
    let handler = new filterHandler();
    handler.reset();
    handler.filter($('.project').val(),$('.type').val());

    $('#remove-typefilter-button').click(function (e) {
      if($('.project').val() == null ) {
        handler.reset();
        handler.RemoveRemoveFilterOption(false,true);
      } else {
        handler.removeOption(true,false,$('.project').val());
      } 
    });

    $('#remove-projectfilter-button').click(function (e) { 
      if($('.type').val() == null ) {
        handler.reset();
        handler.RemoveRemoveFilterOption(true,false);
      } else {
        handler.removeOption(false,true,$('.type').val());
      }
        
    });
  });
});


    
