var wichTab = $('.wich-tab-info').val()

$("#scroll-trigger").click(function() {
    console.log("trigger called");
    $('html, body').animate({
      scrollTop: $("#scroll-to").offset().top
    }, 2000);
}) 

if ( wichTab.length < 1 ){
    openTab('start');
}

function smoothScrollto(id){
        id = "#" + id;
        console.log("smooth scroll to" + id);

        $('html, body').animate({
              scrollTop: $(id).offset().top
        }, 2000);
   
}
function openForm(ID,type,e){

    e.preventDefault();
    if (type == 'item'){
        smoothScrollto("form");
        $('#add-trigger').hide();
        $('#close-trigger').show();
        $('#load').show();
    } else{
        smoothScrollto("form-additional");
        $('#add-trigger-additional').hide();
        $('#close-trigger-additional').show();
        $('#load-additional').show();
    }

    if (ID == 0){
        load_add_form(e);
    } else{
        load_update_form(ID,e);
    }
}

$('.tab').click(function (e) { 
    var type = $(this).attr('data-id');
    openTab(type);
    if (type == "project"){
        $('#project').show();
    }
});

$('#add-trigger').show();
$('#close-trigger').hide();


$('#add-trigger').click(function (e) {
    openForm(0,'item',e);

    });

    $('#add-trigger-additional').click(function (e) {
    openForm(0,'additional',e);

    });


function load_update_form(id,e) {
e.preventDefault();
let tempObj = new dataBaseObject();

$(tempObj.getformularID()).load(tempObj.getformularLink(), function() {                
        let templink = new link();
        let tempitem = new item();    

        if ($('.wich-tab-info').val() == 'item'){
            tempitem.loadFormdata(id);
            templink.getFormSelectData();
        } else if ($('.wich-tab-info').val() == 'project') {
            templink.loadFormdata(id);
        }
        
        $(' #close-trigger').click(function (e) {
            closeForm('item');
        });

        $(' #close-trigger-additional').click(function (e) {
            closeForm('additional');
        });

        $('#remove-img').click(function (e) {
                templink.removeImage();
        });
            
        $('#save_or_edit').click(function (e) { 
            e.preventDefault();
            let dataforDB = tempObj.createFittingObj();
            dataforDB.saveData();

            var OK = dataforDB.checkRequierdFields();
            if (OK){
                dataforDB.toDB();
                closeForm(tempObj.wichTab());
            } else {
                alert("Please make sure to fill out the required fields");
                e.preventDefault();
            }
        });
});
}

function load_add_form(e) {
    e.preventDefault();
    let tempObj = new dataBaseObject();

    $(tempObj.getformularID()).load(tempObj.getformularLink(), function() {
        $('#id').val(0); 

        let dataHandler = tempObj.createFittingObj();
        dataHandler.getFormSelectData();
        $('#logo-preview').hide();
        $('#remove-img').hide();
        $('#logo-info').hide();


        $('#save_or_edit').click(function (e) { 
            e.preventDefault();
            let DataforDB = tempObj.createFittingObj();
            DataforDB.saveData();
            var OK = DataforDB.checkRequierdFields();
            if (OK){
                DataforDB.toDB();
            } else {
                alert("Please make sure to fill out the required fields");
                e.preventDefault();
            }
        });

        $('#close-trigger').click(function (e) {
            closeForm(tempObj.wichTab());
        });

        $('#close-trigger-additional').click(function (e) {
            closeForm(tempObj.wichTab());
        });

    });
}



function noTabsAtStart(tabs,amountOfTabs){
    $('#item').hide();
    $('#project').hide();

}

function openAdditionalTab(){
    $('#item').hide();
    $('#project').show();
}

function openItemTab(){
    console.warn("opened ITEM tab");
   $('#project').hide();
    $('#item').show();
}

function setWichTabInfo(type){
    $('.wich-tab-info').val(type);
}

function openTab(type) {
   

    if (type == 'item'){
        setWichTabInfo('item');
        openItemTab();
        let itemtable = new item();
        showData(itemtable);
    }else if (type == 'start'){
        setWichTabInfo('project');
        openAdditionalTab();
        let linktable = new link();
        showData(linktable); 
    }  else{        
       location.reload();
    }
}

function writeToDB(link){
    link.action = link.chooseAction();
    $.ajax({
        type: "post",
        url: "api.php?action="+link.action,
        data: {
            id: link.ID,
            name: link.name,
            url: link.url,
            project: link.project_ID,
            type: link.type_ID,
            status: link.status,
        },
        dataType: "json",

    });

    openTab('item');
    
}

function ID_to_name(ID){
        $.ajax({
            async:false,//otherwise I am not able to save data in the variabel
            url: "api.php?action=getID&ID=" + ID,
            success: function(response) {
                name = response;
            }

        });

        return name;
}

function closeForm(type){
    if (type == 'item'){
        $('#close-trigger').hide();
        $('#add-trigger').show();
        $('#load').hide();

    } else {
        $('#close-trigger-additional').hide();
        $('#add-trigger-additional').show();
        $('#load-additional').hide();
    }
}

function checkOpenTab(){
    if ($("").is(":visible")){
        
    }
}

function showData(dataBaseObject){
   $.getJSON("./api.php?type=" + dataBaseObject.getClassName(), function (response) {
    dataBaseObject.dataJSON = response;
    dataBaseObject.renderData();
    dataBaseObject.fillData();
    

    $('.delete-item').click(function (e) {
        e.preventDefault();
        
        if ($('.wich-tab-info').val() == 'item'){
            let objToDelete = new item($(this).parent().attr('data-id'));
            objToDelete.delete();
        } else {
            let objToDelete = new link($(this).parent().attr('data-id'));
            objToDelete.delete();
        }
    });

    $('.1').html('<i class="fas fa-toggle-on"></i><input class="status-val" type="hidden" value="1">');
    $('.0').html('<i class="fas fa-toggle-off"></i><input class="status-val" type="hidden" value="0">');


    $('#add-trigger-additional').show();
    $('#close-trigger-additional').hide();
    
    $('.edit-item').click(function (e) {
        e.preventDefault();
        var id = $(this).parent().attr('data-id');
        
        var tabOpen = $('.wich-tab-info').val();
        openForm(id,tabOpen,e);                        
    });

    $('.status-info').click(function (e) {
        var idOfElement = "#" + $(this).attr("data-id") + ".status-val";

        if ($('.wich-tab-info').val() == 'item'){
            let objtoChange = new item();
            objtoChange.status = $(idOfElement).val();
            objtoChange.ID = $(this).attr("data-id");
            objtoChange.changeStatus();
        } else {
            let objtoChange = new link();
            objtoChange.status = $(idOfElement).val();   
            objtoChange.ID = $(this).attr("data-id");
            objtoChange.changeStatus();

        }

        //changeStatus($(this).attr("data-id"), status);  
        //showData();
    });
});
}



