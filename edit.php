<?php
include './data/database_connection.php';
include './data/functions.php';


if ($_SESSION['loggedin'] == 'yes'){

} else if ($_SESSION['loggedin'] == 'no'){
    header("Location: login.php");

}

if(!isset($_SESSION['loggedin'])){
  $_SESSION['loggedin'] = 'no';

} 

if(!isset($_SESSION['blocked'])){
  $_SESSION['blocked'] = 'no';

} 

?>
<head>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">

            <!-- jquery-->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

			<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
			<link rel="icon" href="/favicon.ico" type="image/x-icon">
			
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="./css/basic-settings.css">
            <link rel="stylesheet" href="./css/flexbox.css">
            <link rel="stylesheet" href="./css/header.css">
            <link rel="stylesheet" href="./css/table.css">
            <link rel="stylesheet" href="./css/form.css">
            <link rel="stylesheet" href="./css/popup.css" id="popup-styling-css">


            <!---jquery---->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
            
            <!---tablesorter---->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.min.js" integrity="sha512-qzgd5cYSZcosqpzpn7zF2ZId8f/8CHmFKZ8j7mU4OUXTNRd5g+ZHBPsgKEwoqxCtdQvExE5LprwwPAgoicguNg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

            <!-- jQuery Modal -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>

            <!-- jQuery Modal CSS -->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

            <!---mustache---->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/4.1.0/mustache.min.js"></script>

            <!--font awesome--->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />

            <!----pagination--> 
            <script src="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.5/pagination.min.js" integrity="sha512-1zzZ0ynR2KXnFskJ1C2s+7TIEewmkB2y+5o/+ahF7mwNj9n3PnzARpqalvtjSbUETwx6yuxP5AJXZCpnjEJkQw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
            <!---------------------------> 

            <script src="./js/script.js"></script>
            <script src="./js/tablesorter.js"></script>

            
</head>


<body>

<!--------------nav------------------->
<div id="myNav" class="overlay">
  <a href="javascript:void(0)" class="closebtn closeNav" >&times;</a>
  <div class="overlay-content">
  <img src="./uploads/default.png" alt="">
  <a href="index.php">Home</a>
  <a href="edit.php">Modify</a>
  <a id="logout" href="index.php?logout=true">Logout</a>
  </div>
</div>
		
<span class="hamburger-icon openNav" >&#9776;</span>



<div class="tab-section">
  <a class="tab" data-id="item">ITEM</a>
  <a class="tab" data-id="project">ADDITIONAL</a>
</div>

<input class="wich-tab-info" type="hidden" name="hidden field expresses wich tab is open">

<div id="item">
      <h1>Modify your item</h1>

      <table id="table-to-sort" class=" myTable">
    <thead class="indigo darken-4 white-text">
        <tr>
            <th>Name</th>
            <th>Url</th>
            <th>Project</th>
            <th>Type</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody>
        
    </tbody>
  </table>

<!---pagination bar------->
<p class="center" id="total_reg"></p> 
<ul class="center pagination pager" id="myPager"></ul> 
<!------------------------>

<section id="form">
  <div id="add-trigger"><i class="fas fa-plus-square"></i></div>
  <div id="close-trigger"><i class="fas fa-times"></i></i></div>

  <div id="load"></div>
</section>


<script id="item-template" type="x-tmpl-mustache">
    {{#data}}
        {{#.}}
            <tr>           
                <td>{{name}}</td>
                <td><a href="{{url}}">{{url}}</a></td>
                <td>{{project_ID}}</td>
                <td>{{type_ID}}</td>
                <td data-id="{{ID}}"  class="status-info">
                <input id="{{ID}}" class="status-val" type="hidden" value="{{status}}"></input>
                <p class="{{status}}"></p>
                </td>
                <td data-id="{{ID}}">
                <a class="edit-item">
                  <i class="far fa-edit"></i>
                </a>
                <a class="delete-item">
                  <i class="far fa-trash-alt"></i>
                  </a>
                </td>

                
            </tr>
        {{/.}}
    {{/data}}
  </script>
</div> 




<div id="project">
      <h1>Modify your additional content</h1>
      <table id="table-to-sort" class="tablesorter">
    <thead class="">
        <tr>
            <th></th>
            <th>Name</th>
            <th>type</th>
            <th>Url *</th>
            <th>Description</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody>
        
    </tbody>
  </table>

<section id="form-additional">
<div id="add-trigger-additional"><i class="fas fa-plus-square"></i></div>
<div id="close-trigger-additional"><i class="fas fa-times"></i></i></div>

<div id="load-additional"></div>
</section>


<script id="project-template" type="x-tmpl-mustache">
    {{#data}}
        {{#.}}
            <tr>       
                <td><img src="{{logo}}"></td>    
                <td>{{name}}</td>
                <td>{{type}}</td>
                <td><a href="{{url}}">{{url}}</a></td>
                <td>{{description}}</td>
                <td data-id="{{ID}}"  class="status-info">
                <input id="{{ID}}" class="status-val" type="hidden" value="{{status}}"></input>
                <p class="{{status}}"></p>
                </td>
                <td data-id="{{ID}}">
                <a class="edit-item">
                  <i class="far fa-edit"></i>
                </a>
                <a class="delete-item">
                  <i class="far fa-trash-alt"></i>
                  </a>
                </td>

                
            </tr>
        {{/.}}
    {{/data}}
  </script>
</div> 


</body>
<?php
?>